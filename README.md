# Terraform Opensearch Roles Module
Creates Opensearch roles

## Usage example
```hcl
# main.tf
module "roles" {
  source = "git::https://gitlab.com/terraform_modules3/opensearch/tf_module_opensearch_roles.git?ref=0.1.0"

  roles = [
    {
      name = "manage_indices"
      index_permissions = [
        {
          index_patterns  = ["*"]
          allowed_actions = ["manage"]
        }
      ]
    },
    {
      name = "monitor_indices"
      index_permissions = [
        {
          index_patterns  = ["*"]
          allowed_actions = ["monitor"]
        }
      ]
    },
    {
      name = "logs-viewer"
      cluster_permissions = [
        "cluster_composite_ops_ro",
      ]
      index_permissions = [
        {
          index_patterns = [
            "logs-*",
          ]
          allowed_actions = [
            "read",
            "view_index_metadata",
          ]
        },
        {
          index_patterns = [
            ".kibana"
          ]
          allowed_actions = [
            "read",
          ]
        }
      ]
      tenant_permissions = [
        {
          tenant_patterns = ["global_tenant"]
          allowed_actions = ["kibana_all_read"]
        }
      ]
    },
  ]
}
```
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.1 |
| <a name="requirement_opensearch"></a> [opensearch](#requirement\_opensearch) | >= 2.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_opensearch"></a> [opensearch](#provider\_opensearch) | 2.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [opensearch_role.role](https://registry.terraform.io/providers/opensearch-project/opensearch/latest/docs/resources/role) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_roles"></a> [roles](#input\_roles) | n/a | <pre>list(object({<br>    name                = string<br>    description         = optional(string)<br>    cluster_permissions = optional(set(string))<br>    tenant_permissions = optional(set(object({<br>      tenant_patterns = optional(set(string))<br>      allowed_actions = optional(set(string))<br>    })), [])<br>    index_permissions = optional(list(object({<br>      index_patterns          = optional(set(string))<br>      allowed_actions         = optional(set(string))<br>      document_level_security = optional(string)<br>      field_level_security    = optional(set(string))<br>      masked_fields           = optional(set(string))<br>    })), [])<br>  }))</pre> | `[]` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
