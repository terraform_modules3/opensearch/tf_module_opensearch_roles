resource "opensearch_role" "role" {
  for_each = { for role in var.roles : role.name => role }

  role_name   = each.value.name
  description = each.value.description

  cluster_permissions = each.value.cluster_permissions

  dynamic "tenant_permissions" {
    for_each = each.value.tenant_permissions
    content {
      tenant_patterns = tenant_permissions.value["tenant_patterns"]
      allowed_actions = tenant_permissions.value["allowed_actions"]
    }
  }

  dynamic "index_permissions" {
    for_each = each.value.index_permissions
    content {
      index_patterns          = index_permissions.value["index_patterns"]
      allowed_actions         = index_permissions.value["allowed_actions"]
      document_level_security = index_permissions.value["document_level_security"]
      field_level_security    = index_permissions.value["field_level_security"]
      masked_fields           = index_permissions.value["masked_fields"]
    }
  }
}
