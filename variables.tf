variable "roles" {
  type = list(object({
    name                = string
    description         = optional(string)
    cluster_permissions = optional(set(string))
    tenant_permissions = optional(set(object({
      tenant_patterns = optional(set(string))
      allowed_actions = optional(set(string))
    })), [])
    index_permissions = optional(list(object({
      index_patterns          = optional(set(string))
      allowed_actions         = optional(set(string))
      document_level_security = optional(string)
      field_level_security    = optional(set(string))
      masked_fields           = optional(set(string))
    })), [])
  }))
  default = []
}
